#

```sh
cd ../k8s-web-hello

# install npm
npm init -y

# express
npm install express

npm start

login docker

docker push <name_docker_hub>/k8s-web-node-hello:latest 
docker push <name_docker_hub>/k8s-web-node-hello:0.1

docker build . -t <name_docker_hub>/k8s-web-node-hello:latest -t <name_docker_hub>/k8s-web-node-hello:0.2

docker push <name_docker_hub>/k8s-web-node-hello --all-tags
```

```sh

kubectl apply -f deployment.yml -f service.yml
```
