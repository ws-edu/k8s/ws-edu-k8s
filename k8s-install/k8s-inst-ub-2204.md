


## HOSTNAME
```sh
hostnamectl set-hostname k8s{m1,m2,m3,w1,w2}.local
```

## SSH
```sh
echo "PermitRootLogin yes" > /etc/ssh/sshd_config.d/01-permitroot.conf
service sshd restart

rm /etc/ssh/sshd_config.d/01-permitroot.conf
service sshd restart
```

## vim /etc/netplan/00-installer-config.yaml 
```sh
# This is the network config written by 'subiquity'
network:
  ethernets:
    ens18:
      addresses:
      - 10.101.2.XXX/24
      nameservers:
        addresses:
        - 10.101.2.254
        - 8.8.8.8
        search:
        - k8s.local
      routes:
      - to: default
        via: 10.101.2.254
  version: 2
```

## HOSTS (DNS)
```sh
cat > /etc/hosts <<EOF
127.0.0.1       localhost

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

# Cluster nodes
10.101.2.131 k8sm1
10.101.2.132 k8sm2
10.101.2.133 k8sm3
10.101.2.134 k8sm4
10.101.2.135 k8sw1
10.101.2.136 k8sw2
10.101.2.137 k8sw3
10.101.2.138 k8sw4
EOF
```

```sh
apt install -y curl wget gnupg sudo iptables tmux keepalived haproxy
```

```sh
reboot
```

## Install and configure prerequisites
## Предварительная подготовка Linux для использования Kubernetes

Согласно официальной документации(https://kubernetes.io/docs/setup/production-environment/container-runtimes/), 
для работы Kubernetes необходимо разрешить маршрутизацию IPv4 трафика, 
настроить возможность iptables видеть трафик, передаваемый в режиме моста, 
а также отключить файлы подкачки. 

```sh
# Настройка автозагрузки и запуск модуля ядра br_netfilter и overlay
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter
```

```sh
# Разрешение маршрутизации IP-трафика
# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

sysctl -f /etc/sysctl.d/k8s.conf
sysctl net.bridge.bridge-nf-call-iptables net.bridge.bridge-nf-call-ip6tables net.ipv4.ip_forward
reboot
```

```sh
# Отключение файла подкачки
swapoff -a
sed -i '/ swap / s/^/#/' /etc/fstab
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
```


# Установка kubeadm и kubectl

kubectl(https://kubernetes.io/docs/reference/kubectl/) – основная утилита командной строки для управления кластером Kubernetes, 
kubeadm(https://kubernetes.io/docs/reference/setup-tools/kubeadm/) – утилита для развертывания кластера Kubernetes. 
Установка данных утилит осуществляется в соответствии с официальным руководством(https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl).


```sh
apt update && apt upgrade -y

sudo apt install curl apt-transport-https -y
# Затем добавьте GPG-ключ командой:
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
# Добавьте репозиторий Kubernetes:
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
#
apt update && apt upgrade -y
# Установите необходимое программное обеспечение:
sudo apt -y install vim git curl wget kubelet kubeadm kubectl
# Отключите автоматическое обновление kubelet, kubeadm и kubectl:
sudo apt-mark hold kubelet kubeadm kubectl
# Включите службу kubelet:
sudo systemctl enable --now kubelet
#
kubeadm version
```

## CRI-O


## Containerd

## Установка осуществляется в соответствии с официальным руководством.
(https://github.com/containerd/containerd/blob/main/docs/getting-started.md)

### UBUNTU
```sh
# Настройте постоянную загрузку модулей Containerd
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF
# Перезагрузите sysctl 
sudo sysctl --system
# 
sudo apt install curl gnupg2 software-properties-common apt-transport-https ca-certificates -y
# Добавьте GPG-ключ
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# Добавьте необходимый репозиторий при помощи команды:
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# Установите Сontainerd:
apt update && apt upgrade -y
sudo apt install containerd.io -y
# root:
sudo su -
# Создайте новый каталог для Сontainerd:
mkdir -p /etc/containerd
# Создайте файл конфигурации:
containerd config default>/etc/containerd/config.toml
# Выйдите из-под пользователя root:
exit
# Перезапустите Containerd:
sudo systemctl restart containerd
# Включите автоматический запуск службы Containerd:
sudo systemctl enable containerd
#
reboot
#
crictl --runtime-endpoint unix:///var/run/containerd/containerd.sock version
#
ctr images pull docker.io/library/hello-world:latest
ctr run docker.io/library/hello-world:latest hello-world
#

```





# vim /etc/keepalived/keepalived.conf

global_defs {
    enable_script_security
    script_user nobody
}

vrrp_script check_apiserver {
  script "/etc/keepalived/check_apiserver.sh"
  interval 3
}

vrrp_instance VI_1 {
    state BACKUP
    interface ens18
    virtual_router_id 5
    priority 100
    advert_int 1
    nopreempt
    authentication {
        auth_type PASS
        auth_pass psWS#19$76
    }
    virtual_ipaddress {
        10.101.2.210
    }
    track_script {
        check_apiserver
    }
}


# vim /etc/keepalived/check_apiserver.sh
```sh
#!/bin/sh
# File: /etc/keepalived/check_apiserver.sh

APISERVER_VIP=10.101.2.210
APISERVER_DEST_PORT=8888
PROTO=http

errorExit() {
    echo "*** $*" 1>&2
    exit 1
}

curl --silent --max-time 2 --insecure ${PROTO}://localhost:${APISERVER_DEST_PORT}/ -o /dev/null || errorExit "Error GET ${PROTO}://localhost:${APISERVER_DEST_PORT}/"
if ip addr | grep -q ${APISERVER_VIP}; then
    curl --silent --max-time 2 --insecure ${PROTO}://${APISERVER_VIP}:${APISERVER_DEST_PORT}/ -o /dev/null || errorExit "Error GET ${PROTO}://${APISERVER_VIP}:${APISERVER_DEST_PORT}/"
fi
```

```sh
chmod +x /etc/keepalived/check_apiserver.sh
systemctl enable keepalived
systemctl start keepalived
```


# mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg.copy
# vim /etc/haproxy/haproxy.cfg
```sh
# File: /etc/haproxy/haproxy.cfg
#---------------------------------------------------------------------
# Global settings
#---------------------------------------------------------------------
global
    log /dev/log local0
    log /dev/log local1 notice
    daemon

#---------------------------------------------------------------------
# common defaults that all the 'listen' and 'backend' sections will
# use if not designated in their block
#---------------------------------------------------------------------
defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option http-server-close
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 1
    timeout http-request    10s
    timeout queue           20s
    timeout connect         5s
    timeout client          20s
    timeout server          20s
    timeout http-keep-alive 10s
    timeout check           10s

#---------------------------------------------------------------------
# apiserver frontend which proxys to the control plane nodes
#---------------------------------------------------------------------
frontend apiserver
    bind *:8888
    mode tcp
    option tcplog
    default_backend apiserver

#---------------------------------------------------------------------
# round robin balancing for apiserver
#---------------------------------------------------------------------
backend apiserver
    option httpchk GET /healthz
    http-check expect status 200
    mode tcp
    option ssl-hello-chk
    balance     roundrobin
        server node1 10.101.2.131:6443 check
        server node2 10.101.2.132:6443 check
        server node3 10.101.2.133:6443 check
```

```sh
systemctl enable haproxy
systemctl restart haproxy
```

