# Podman

```url
https://github.com/containers/podman/releases/tag/v4.6.0
```


```sh
cd ../minikube

podman machine init --cpus 2 --memory 2048 --disk-size 20
podman machine start
podman system connection default podman-machine-default-root
podman info

minikube start --driver=podman --container-runtime=containerd

minikube ssh
sudo ctr -n k8s.io containers list

```


```sh
cd ../podman
kubectl apply -f ../k8s-web-to-nginx/k8s-web-to-nginx.yml -f ../k8s-web-to-nginx/nginx.yml 
kubectl delete -f ../k8s-web-to-nginx/k8s-web-to-nginx.yml -f ../k8s-web-to-nginx/nginx.yml 

```



```sh
minikube stop
minikube delete

podman machine stop
podman machine rm
```