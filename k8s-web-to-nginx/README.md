#

```sh
cd k8s-web-to-nginx
docker build . -t <name_docker_hub>/k8s-web-to-nginx:latest
docker push <name_docker_hub>/k8s-web-to-nginx --all-tags

kubectl apply -f nginx.yml -f k8s-web-to-nginx.yml
```

```sh
docker tag <name_docker_hub>/k8s-web-to-nginx:latest <name_docker_hub>/k8s-web-to-nginx:0.1
docker push <name_docker_hub>/k8s-web-to-nginx:0.1
```
#0.2
```sh
docker build . -t <name_docker_hub>/k8s-web-to-nginx:0.2
docker push <name_docker_hub>/k8s-web-to-nginx --all-tags
```

```sh
kubectl apply -f k8s-web-to-nginx.yml -f nginx.yml 
```

# 0.3
```sh
docker build . -t <name_docker_hub>/k8s-web-to-nginx:0.3
docker push <name_docker_hub>/k8s-web-to-nginx --all-tags

kubectl apply -f k8s-web-to-nginx.yml -f nginx.yml 
```

# delete
```sh
kubectl delete -f k8s-web-to-nginx.yml -f nginx.yml 
```
