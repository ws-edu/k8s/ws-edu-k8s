# ws-edu-k8s

```sh

minikube -i
minikube ssh



# PODS
kubectl run ws-pods --image=nginx
kubectl get pods
kubectl describe pods <>
kubectl get pods -o wide
kubectl delete pod <>

# DEPLOYMENT [deploy]
kubectl create deploy ws-deploy --image=nginx
kubectl create deploy ws-deploy --image=nginx:latest --replicas=X

kubectl get deploy
kubectl describe deploy <>

kubectl scale deploy ws-deploy --relicas=x
kubectl rollout status deploy <>

kubectl delete deploy <>

# SERVICES [svc]
kubectl get services
kubectl describe svc <>

# ClientIP
kubectl expose deploy <name> --type=ClientIP --port=<out> --target-port=<in>

# NodePort
kubectl expose deploy <name> --type=NodePort --port=<out> --target-port=<in>
minikube service <> --url

# ClusterIP
minikube tunnel
kubectl expose deploy <name> --type=LoadBalancer --port=<out> --target-port=<in>

kubectl set image deploy <name> <image>=<name>/<image>:latest 

```



```sh
$ sudo apt update
$ sudo apt upgrade -y
$ sudo reboot

# install docker
$ sudo apt install ca-certificates curl gnupg wget apt-transport-https -y
$ sudo install -m 0755 -d /etc/apt/keyrings
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
$ sudo chmod a+r /etc/apt/keyrings/docker.gpg
$ echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
$ sudo apt update

$ sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

$ sudo usermod -aG docker $USER
$ newgrp docker

# Download and Install Minikube Binary
$ curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
$ sudo install minikube-linux-amd64 /usr/local/bin/minikube

$ minikube version

# Install Kubectl tool
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

$ chmod +x kubectl
$ sudo mv kubectl /usr/local/bin/

$  kubectl version -o yaml

$ minikube start --driver=docker

$ minikube status

$ kubectl get nodes
$ kubectl cluster-info

$ kubectl create deployment nginx-web --image=nginx
$ kubectl expose deployment nginx-web --type NodePort --port=80
$ kubectl get deployment,pod,svc

# Managing Minikube Addons
$ minikube addons list

$ minikube addons enable dashboard
$ minikube addons enable ingress

# Managing Minikube Cluster
$ minikube stop
$ minikube start

$ minikube delete

```


